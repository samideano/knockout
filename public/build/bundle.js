
(function(l, r) { if (l.getElementById('livereloadscript')) return; r = l.createElement('script'); r.async = 1; r.src = '//' + (window.location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1'; r.id = 'livereloadscript'; l.getElementsByTagName('head')[0].appendChild(r) })(window.document);
var app = (function () {
    'use strict';

    function noop() { }
    function add_location(element, file, line, column, char) {
        element.__svelte_meta = {
            loc: { file, line, column, char }
        };
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }
    function validate_store(store, name) {
        if (store != null && typeof store.subscribe !== 'function') {
            throw new Error(`'${name}' is not a store with a 'subscribe' method`);
        }
    }
    function subscribe(store, ...callbacks) {
        if (store == null) {
            return noop;
        }
        const unsub = store.subscribe(...callbacks);
        return unsub.unsubscribe ? () => unsub.unsubscribe() : unsub;
    }
    function get_store_value(store) {
        let value;
        subscribe(store, _ => value = _)();
        return value;
    }
    function component_subscribe(component, store, callback) {
        component.$$.on_destroy.push(subscribe(store, callback));
    }
    function set_store_value(store, ret, value = ret) {
        store.set(value);
        return ret;
    }

    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function destroy_each(iterations, detaching) {
        for (let i = 0; i < iterations.length; i += 1) {
            if (iterations[i])
                iterations[i].d(detaching);
        }
    }
    function element(name) {
        return document.createElement(name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function empty() {
        return text('');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else if (node.getAttribute(attribute) !== value)
            node.setAttribute(attribute, value);
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_style(node, key, value, important) {
        node.style.setProperty(key, value, important ? 'important' : '');
    }
    function toggle_class(element, name, toggle) {
        element.classList[toggle ? 'add' : 'remove'](name);
    }
    function custom_event(type, detail) {
        const e = document.createEvent('CustomEvent');
        e.initCustomEvent(type, false, false, detail);
        return e;
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }
    function get_current_component() {
        if (!current_component)
            throw new Error(`Function called outside component initialization`);
        return current_component;
    }
    function createEventDispatcher() {
        const component = get_current_component();
        return (type, detail) => {
            const callbacks = component.$$.callbacks[type];
            if (callbacks) {
                // TODO are there situations where events could be dispatched
                // in a server (non-DOM) environment?
                const event = custom_event(type, detail);
                callbacks.slice().forEach(fn => {
                    fn.call(component, event);
                });
            }
        };
    }
    // TODO figure out if we still want to support
    // shorthand events, or if we want to implement
    // a real bubbling mechanism
    function bubble(component, event) {
        const callbacks = component.$$.callbacks[event.type];
        if (callbacks) {
            callbacks.slice().forEach(fn => fn(event));
        }
    }

    const dirty_components = [];
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    let flushing = false;
    const seen_callbacks = new Set();
    function flush() {
        if (flushing)
            return;
        flushing = true;
        do {
            // first, call beforeUpdate functions
            // and update components
            for (let i = 0; i < dirty_components.length; i += 1) {
                const component = dirty_components[i];
                set_current_component(component);
                update(component.$$);
            }
            dirty_components.length = 0;
            while (binding_callbacks.length)
                binding_callbacks.pop()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            for (let i = 0; i < render_callbacks.length; i += 1) {
                const callback = render_callbacks[i];
                if (!seen_callbacks.has(callback)) {
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                    callback();
                }
            }
            render_callbacks.length = 0;
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
        flushing = false;
        seen_callbacks.clear();
    }
    function update($$) {
        if ($$.fragment !== null) {
            $$.update();
            run_all($$.before_update);
            const dirty = $$.dirty;
            $$.dirty = [-1];
            $$.fragment && $$.fragment.p($$.ctx, dirty);
            $$.after_update.forEach(add_render_callback);
        }
    }
    const outroing = new Set();
    let outros;
    function group_outros() {
        outros = {
            r: 0,
            c: [],
            p: outros // parent group
        };
    }
    function check_outros() {
        if (!outros.r) {
            run_all(outros.c);
        }
        outros = outros.p;
    }
    function transition_in(block, local) {
        if (block && block.i) {
            outroing.delete(block);
            block.i(local);
        }
    }
    function transition_out(block, local, detach, callback) {
        if (block && block.o) {
            if (outroing.has(block))
                return;
            outroing.add(block);
            outros.c.push(() => {
                outroing.delete(block);
                if (callback) {
                    if (detach)
                        block.d(1);
                    callback();
                }
            });
            block.o(local);
        }
    }

    const globals = (typeof window !== 'undefined'
        ? window
        : typeof globalThis !== 'undefined'
            ? globalThis
            : global);
    function create_component(block) {
        block && block.c();
    }
    function mount_component(component, target, anchor) {
        const { fragment, on_mount, on_destroy, after_update } = component.$$;
        fragment && fragment.m(target, anchor);
        // onMount happens before the initial afterUpdate
        add_render_callback(() => {
            const new_on_destroy = on_mount.map(run).filter(is_function);
            if (on_destroy) {
                on_destroy.push(...new_on_destroy);
            }
            else {
                // Edge case - component was destroyed immediately,
                // most likely as a result of a binding initialising
                run_all(new_on_destroy);
            }
            component.$$.on_mount = [];
        });
        after_update.forEach(add_render_callback);
    }
    function destroy_component(component, detaching) {
        const $$ = component.$$;
        if ($$.fragment !== null) {
            run_all($$.on_destroy);
            $$.fragment && $$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            $$.on_destroy = $$.fragment = null;
            $$.ctx = [];
        }
    }
    function make_dirty(component, i) {
        if (component.$$.dirty[0] === -1) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty.fill(0);
        }
        component.$$.dirty[(i / 31) | 0] |= (1 << (i % 31));
    }
    function init(component, options, instance, create_fragment, not_equal, props, dirty = [-1]) {
        const parent_component = current_component;
        set_current_component(component);
        const prop_values = options.props || {};
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props,
            update: noop,
            not_equal,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            before_update: [],
            after_update: [],
            context: new Map(parent_component ? parent_component.$$.context : []),
            // everything else
            callbacks: blank_object(),
            dirty
        };
        let ready = false;
        $$.ctx = instance
            ? instance(component, prop_values, (i, ret, ...rest) => {
                const value = rest.length ? rest[0] : ret;
                if ($$.ctx && not_equal($$.ctx[i], $$.ctx[i] = value)) {
                    if ($$.bound[i])
                        $$.bound[i](value);
                    if (ready)
                        make_dirty(component, i);
                }
                return ret;
            })
            : [];
        $$.update();
        ready = true;
        run_all($$.before_update);
        // `false` as a special case of no DOM component
        $$.fragment = create_fragment ? create_fragment($$.ctx) : false;
        if (options.target) {
            if (options.hydrate) {
                const nodes = children(options.target);
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.l(nodes);
                nodes.forEach(detach);
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.c();
            }
            if (options.intro)
                transition_in(component.$$.fragment);
            mount_component(component, options.target, options.anchor);
            flush();
        }
        set_current_component(parent_component);
    }
    class SvelteComponent {
        $destroy() {
            destroy_component(this, 1);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set() {
            // overridden by instance, if it has props
        }
    }

    function dispatch_dev(type, detail) {
        document.dispatchEvent(custom_event(type, Object.assign({ version: '3.22.2' }, detail)));
    }
    function append_dev(target, node) {
        dispatch_dev("SvelteDOMInsert", { target, node });
        append(target, node);
    }
    function insert_dev(target, node, anchor) {
        dispatch_dev("SvelteDOMInsert", { target, node, anchor });
        insert(target, node, anchor);
    }
    function detach_dev(node) {
        dispatch_dev("SvelteDOMRemove", { node });
        detach(node);
    }
    function listen_dev(node, event, handler, options, has_prevent_default, has_stop_propagation) {
        const modifiers = options === true ? ["capture"] : options ? Array.from(Object.keys(options)) : [];
        if (has_prevent_default)
            modifiers.push('preventDefault');
        if (has_stop_propagation)
            modifiers.push('stopPropagation');
        dispatch_dev("SvelteDOMAddEventListener", { node, event, handler, modifiers });
        const dispose = listen(node, event, handler, options);
        return () => {
            dispatch_dev("SvelteDOMRemoveEventListener", { node, event, handler, modifiers });
            dispose();
        };
    }
    function attr_dev(node, attribute, value) {
        attr(node, attribute, value);
        if (value == null)
            dispatch_dev("SvelteDOMRemoveAttribute", { node, attribute });
        else
            dispatch_dev("SvelteDOMSetAttribute", { node, attribute, value });
    }
    function set_data_dev(text, data) {
        data = '' + data;
        if (text.data === data)
            return;
        dispatch_dev("SvelteDOMSetData", { node: text, data });
        text.data = data;
    }
    function validate_each_argument(arg) {
        if (typeof arg !== 'string' && !(arg && typeof arg === 'object' && 'length' in arg)) {
            let msg = '{#each} only iterates over array-like objects.';
            if (typeof Symbol === 'function' && arg && Symbol.iterator in arg) {
                msg += ' You can use a spread to convert this iterable into an array.';
            }
            throw new Error(msg);
        }
    }
    function validate_slots(name, slot, keys) {
        for (const slot_key of Object.keys(slot)) {
            if (!~keys.indexOf(slot_key)) {
                console.warn(`<${name}> received an unexpected slot "${slot_key}".`);
            }
        }
    }
    class SvelteComponentDev extends SvelteComponent {
        constructor(options) {
            if (!options || (!options.target && !options.$$inline)) {
                throw new Error(`'target' is a required option`);
            }
            super();
        }
        $destroy() {
            super.$destroy();
            this.$destroy = () => {
                console.warn(`Component was already destroyed`); // eslint-disable-line no-console
            };
        }
        $capture_state() { }
        $inject_state() { }
    }

    const subscriber_queue = [];
    /**
     * Creates a `Readable` store that allows reading by subscription.
     * @param value initial value
     * @param {StartStopNotifier}start start and stop notifications for subscriptions
     */
    function readable(value, start) {
        return {
            subscribe: writable(value, start).subscribe,
        };
    }
    /**
     * Create a `Writable` store that allows both updating and reading by subscription.
     * @param {*=}value initial value
     * @param {StartStopNotifier=}start start and stop notifications for subscriptions
     */
    function writable(value, start = noop) {
        let stop;
        const subscribers = [];
        function set(new_value) {
            if (safe_not_equal(value, new_value)) {
                value = new_value;
                if (stop) { // store is ready
                    const run_queue = !subscriber_queue.length;
                    for (let i = 0; i < subscribers.length; i += 1) {
                        const s = subscribers[i];
                        s[1]();
                        subscriber_queue.push(s, value);
                    }
                    if (run_queue) {
                        for (let i = 0; i < subscriber_queue.length; i += 2) {
                            subscriber_queue[i][0](subscriber_queue[i + 1]);
                        }
                        subscriber_queue.length = 0;
                    }
                }
            }
        }
        function update(fn) {
            set(fn(value));
        }
        function subscribe(run, invalidate = noop) {
            const subscriber = [run, invalidate];
            subscribers.push(subscriber);
            if (subscribers.length === 1) {
                stop = start(set) || noop;
            }
            run(value);
            return () => {
                const index = subscribers.indexOf(subscriber);
                if (index !== -1) {
                    subscribers.splice(index, 1);
                }
                if (subscribers.length === 0) {
                    stop();
                    stop = null;
                }
            };
        }
        return { set, update, subscribe };
    }

    const params = new URLSearchParams(window.location.search);

    const lang = readable(params.get('l') || 'en');

    const title = readable({en: 'Knockout', fr: 'Ti-coupe', pt: 'Copinha'}[get_store_value(lang)]);

    const background = readable(params.get('bg'));

    const initialContenders = readable(
      decodeURIComponent(params.get('c') || '')
        .replace(/^\?/, '')
        .split('|')
    );

    const screen = writable('tournament');

    const champion = writable();

    /* src/Examples.svelte generated by Svelte v3.22.2 */
    const file = "src/Examples.svelte";

    function get_each_context(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[3] = list[i];
    	return child_ctx;
    }

    // (25:2) {#each examples as ex}
    function create_each_block(ctx) {
    	let div;
    	let a;
    	let t_value = /*ex*/ ctx[3].title + "";
    	let t;
    	let a_href_value;

    	const block = {
    		c: function create() {
    			div = element("div");
    			a = element("a");
    			t = text(t_value);
    			attr_dev(a, "href", a_href_value = "?c=" + /*ex*/ ctx[3][/*$lang*/ ctx[0]] + "&bg=" + /*ex*/ ctx[3].background + "&l=" + /*$lang*/ ctx[0]);
    			add_location(a, file, 25, 25, 1807);
    			attr_dev(div, "class", "example svelte-tqonbn");
    			add_location(div, file, 25, 4, 1786);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, a);
    			append_dev(a, t);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*$lang*/ 1 && a_href_value !== (a_href_value = "?c=" + /*ex*/ ctx[3][/*$lang*/ ctx[0]] + "&bg=" + /*ex*/ ctx[3].background + "&l=" + /*$lang*/ ctx[0])) {
    				attr_dev(a, "href", a_href_value);
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block.name,
    		type: "each",
    		source: "(25:2) {#each examples as ex}",
    		ctx
    	});

    	return block;
    }

    function create_fragment(ctx) {
    	let main;
    	let h1;
    	let t0;
    	let span0;
    	let t1;
    	let span1;
    	let t3;
    	let h3;

    	let t4_value = ({
    		en: "Examples",
    		fr: "Exemples",
    		pt: "Exemplos"
    	})[/*$lang*/ ctx[0]] + "";

    	let t4;
    	let t5;
    	let t6;
    	let each_value = /*examples*/ ctx[2];
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block(get_each_context(ctx, each_value, i));
    	}

    	const block = {
    		c: function create() {
    			main = element("main");
    			h1 = element("h1");
    			t0 = text("🏆 ");
    			span0 = element("span");
    			t1 = text(/*$title*/ ctx[1]);
    			span1 = element("span");
    			span1.textContent = "!";
    			t3 = space();
    			h3 = element("h3");
    			t4 = text(t4_value);
    			t5 = text(":");
    			t6 = space();

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr_dev(span0, "class", "text svelte-tqonbn");
    			add_location(span0, file, 22, 23, 1611);
    			attr_dev(span1, "class", "exclamation svelte-tqonbn");
    			add_location(span1, file, 22, 57, 1645);
    			attr_dev(h1, "class", "title svelte-tqonbn");
    			add_location(h1, file, 22, 2, 1590);
    			add_location(h3, file, 23, 2, 1687);
    			attr_dev(main, "class", "examples svelte-tqonbn");
    			add_location(main, file, 21, 0, 1564);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, main, anchor);
    			append_dev(main, h1);
    			append_dev(h1, t0);
    			append_dev(h1, span0);
    			append_dev(span0, t1);
    			append_dev(h1, span1);
    			append_dev(main, t3);
    			append_dev(main, h3);
    			append_dev(h3, t4);
    			append_dev(h3, t5);
    			append_dev(main, t6);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(main, null);
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*$title*/ 2) set_data_dev(t1, /*$title*/ ctx[1]);

    			if (dirty & /*$lang*/ 1 && t4_value !== (t4_value = ({
    				en: "Examples",
    				fr: "Exemples",
    				pt: "Exemplos"
    			})[/*$lang*/ ctx[0]] + "")) set_data_dev(t4, t4_value);

    			if (dirty & /*examples, $lang*/ 5) {
    				each_value = /*examples*/ ctx[2];
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(main, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(main);
    			destroy_each(each_blocks, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance($$self, $$props, $$invalidate) {
    	let $lang;
    	let $title;
    	validate_store(lang, "lang");
    	component_subscribe($$self, lang, $$value => $$invalidate(0, $lang = $$value));
    	validate_store(title, "title");
    	component_subscribe($$self, title, $$value => $$invalidate(1, $title = $$value));

    	const examples = [
    		{
    			title: ({ en: "Fruits", fr: "Fruits", pt: "Frutas" })[$lang],
    			background: "https://wallpapercave.com/wp/wp3145270.jpg",
    			en: "🍌 Banana|🍎 Apple|🍐 Pear|🍍 Pineapple|🍇 Grapes|🍋 Lemon|🍓 Strawberry|🥝 Kiwi|🍑 Peach|🍊 Orange|🍊 Tangerine|🍉 Watermelon|🍏 Soursop|🍈 Jackfruit|🔵 Blueberry|🍏 Sugar-apple",
    			fr: "🍌 Banane|🍎 Pomme|🍐 Poir|🍍 Ananas|🍇 Raisin|🍋 Citron|🍓 Fraise|🥝 Kiwi|🍑 Pêche|🍊 Orange|🍊 Tangerine|🍉 Pastèque|🍏 Corossol|🍈 Pomme Jacque|🔵 Myrtille|🍏 Pocanelle",
    			pt: "🍌 Banana|🍎 Maçã|🍐 Pera|🍍 Abacaxi|🍇 Uva|🍋 Limão|🍓 Morango|🥝 Kiwi|🍑 Pêssego|🍊 Laranja|🍊 Tangerina|🍉 Melancia|🍏 Graviola|🍈 Jaca|🔵 Blueberry|👑 Fruta do Conde"
    		},
    		{
    			title: ({
    				en: "TV Shows",
    				fr: "Séries Télé",
    				pt: "Séries"
    			})[$lang],
    			background: "https://cdn.wallpapersafari.com/9/79/rCRVuM.jpg",
    			en: "Breaking Bad|Better Call Saul|Seinfeld|Game of Thrones|House|ER|Stranger Things|Star Trek: TNG|The Simpsons|Black Mirror|The Walking Dead|The X-Files|Saint Seiya|Futurama|The Office|Outlander",
    			fr: "Breaking Bad|Better Call Saul|Seinfeld|Game of Thrones|House|ER|Stranger Things|Star Trek: TNG|Les Simpson|Black Mirror|The Walking Dead|The X-Files|Saint Seiya|Futurama|The Office|Outlander",
    			pt: "Breaking Bad|Better Call Saul|Seinfeld|Game of Thrones|House|ER|Stranger Things|Star Trek: TNG|Os Simpsons|Black Mirror|The Walking Dead|Arquivo X|Cavaleiros do Zodíaco|Futurama|The Office|Outlander"
    		}
    	];

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Examples> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Examples", $$slots, []);
    	$$self.$capture_state = () => ({ lang, title, examples, $lang, $title });
    	return [$lang, $title, examples];
    }

    class Examples extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance, create_fragment, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Examples",
    			options,
    			id: create_fragment.name
    		});
    	}
    }

    /* src/Pair.svelte generated by Svelte v3.22.2 */
    const file$1 = "src/Pair.svelte";

    function create_fragment$1(ctx) {
    	let div2;
    	let div0;
    	let t0_value = (/*contenders*/ ctx[1][0] || "") + "";
    	let t0;
    	let t1;
    	let div1;
    	let t2_value = (/*contenders*/ ctx[1][1] || "") + "";
    	let t2;
    	let dispose;

    	const block = {
    		c: function create() {
    			div2 = element("div");
    			div0 = element("div");
    			t0 = text(t0_value);
    			t1 = space();
    			div1 = element("div");
    			t2 = text(t2_value);
    			attr_dev(div0, "class", "box svelte-q5afof");
    			add_location(div0, file$1, 18, 2, 437);
    			attr_dev(div1, "class", "box svelte-q5afof");
    			add_location(div1, file$1, 19, 2, 484);
    			attr_dev(div2, "class", "pair svelte-q5afof");
    			toggle_class(div2, "disabled", /*disabled*/ ctx[0]);
    			add_location(div2, file$1, 17, 0, 376);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor, remount) {
    			insert_dev(target, div2, anchor);
    			append_dev(div2, div0);
    			append_dev(div0, t0);
    			append_dev(div2, t1);
    			append_dev(div2, div1);
    			append_dev(div1, t2);
    			if (remount) dispose();
    			dispose = listen_dev(div2, "click", /*onPairClick*/ ctx[2], false, false, false);
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*contenders*/ 2 && t0_value !== (t0_value = (/*contenders*/ ctx[1][0] || "") + "")) set_data_dev(t0, t0_value);
    			if (dirty & /*contenders*/ 2 && t2_value !== (t2_value = (/*contenders*/ ctx[1][1] || "") + "")) set_data_dev(t2, t2_value);

    			if (dirty & /*disabled*/ 1) {
    				toggle_class(div2, "disabled", /*disabled*/ ctx[0]);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div2);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$1.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$1($$self, $$props, $$invalidate) {
    	const dispatch = createEventDispatcher();
    	let { round } = $$props;
    	let { pair } = $$props;
    	let { contenders } = $$props;
    	let { disabled = false } = $$props;

    	function onPairClick() {
    		if (!disabled && contenders[0] && contenders[1]) {
    			$$invalidate(0, disabled = true);
    			dispatch("pairClick", { round, pair, contenders });
    		}
    	}

    	const writable_props = ["round", "pair", "contenders", "disabled"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Pair> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Pair", $$slots, []);

    	$$self.$set = $$props => {
    		if ("round" in $$props) $$invalidate(3, round = $$props.round);
    		if ("pair" in $$props) $$invalidate(4, pair = $$props.pair);
    		if ("contenders" in $$props) $$invalidate(1, contenders = $$props.contenders);
    		if ("disabled" in $$props) $$invalidate(0, disabled = $$props.disabled);
    	};

    	$$self.$capture_state = () => ({
    		createEventDispatcher,
    		dispatch,
    		round,
    		pair,
    		contenders,
    		disabled,
    		onPairClick
    	});

    	$$self.$inject_state = $$props => {
    		if ("round" in $$props) $$invalidate(3, round = $$props.round);
    		if ("pair" in $$props) $$invalidate(4, pair = $$props.pair);
    		if ("contenders" in $$props) $$invalidate(1, contenders = $$props.contenders);
    		if ("disabled" in $$props) $$invalidate(0, disabled = $$props.disabled);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [disabled, contenders, onPairClick, round, pair];
    }

    class Pair extends SvelteComponentDev {
    	constructor(options) {
    		super(options);

    		init(this, options, instance$1, create_fragment$1, safe_not_equal, {
    			round: 3,
    			pair: 4,
    			contenders: 1,
    			disabled: 0
    		});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Pair",
    			options,
    			id: create_fragment$1.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*round*/ ctx[3] === undefined && !("round" in props)) {
    			console.warn("<Pair> was created without expected prop 'round'");
    		}

    		if (/*pair*/ ctx[4] === undefined && !("pair" in props)) {
    			console.warn("<Pair> was created without expected prop 'pair'");
    		}

    		if (/*contenders*/ ctx[1] === undefined && !("contenders" in props)) {
    			console.warn("<Pair> was created without expected prop 'contenders'");
    		}
    	}

    	get round() {
    		throw new Error("<Pair>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set round(value) {
    		throw new Error("<Pair>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get pair() {
    		throw new Error("<Pair>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set pair(value) {
    		throw new Error("<Pair>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get contenders() {
    		throw new Error("<Pair>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set contenders(value) {
    		throw new Error("<Pair>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get disabled() {
    		throw new Error("<Pair>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set disabled(value) {
    		throw new Error("<Pair>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/Round.svelte generated by Svelte v3.22.2 */
    const file$2 = "src/Round.svelte";

    function get_each_context$1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[5] = list[i];
    	child_ctx[7] = i;
    	return child_ctx;
    }

    // (19:4) {#each Array(round) as item, idx}
    function create_each_block$1(ctx) {
    	let current;

    	const pair = new Pair({
    			props: {
    				round: /*round*/ ctx[0],
    				pair: /*idx*/ ctx[7] + 1,
    				contenders: /*contenders*/ ctx[1]
    				? /*contenders*/ ctx[1].slice(/*idx*/ ctx[7] * 2, /*idx*/ ctx[7] * 2 + 2)
    				: []
    			},
    			$$inline: true
    		});

    	pair.$on("pairClick", /*pairClick_handler*/ ctx[4]);

    	const block = {
    		c: function create() {
    			create_component(pair.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(pair, target, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const pair_changes = {};
    			if (dirty & /*round*/ 1) pair_changes.round = /*round*/ ctx[0];

    			if (dirty & /*contenders*/ 2) pair_changes.contenders = /*contenders*/ ctx[1]
    			? /*contenders*/ ctx[1].slice(/*idx*/ ctx[7] * 2, /*idx*/ ctx[7] * 2 + 2)
    			: [];

    			pair.$set(pair_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(pair.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(pair.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(pair, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$1.name,
    		type: "each",
    		source: "(19:4) {#each Array(round) as item, idx}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$2(ctx) {
    	let div1;
    	let h1;
    	let t0;
    	let t1;
    	let div0;
    	let current;
    	let each_value = Array(/*round*/ ctx[0]);
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$1(get_each_context$1(ctx, each_value, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	const block = {
    		c: function create() {
    			div1 = element("div");
    			h1 = element("h1");
    			t0 = text(/*label*/ ctx[2]);
    			t1 = space();
    			div0 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr_dev(h1, "class", "name svelte-1ueei1e");
    			add_location(h1, file$2, 15, 2, 405);
    			attr_dev(div0, "class", "pairs svelte-1ueei1e");
    			add_location(div0, file$2, 17, 2, 438);
    			attr_dev(div1, "class", "round svelte-1ueei1e");
    			add_location(div1, file$2, 14, 0, 383);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div1, anchor);
    			append_dev(div1, h1);
    			append_dev(h1, t0);
    			append_dev(div1, t1);
    			append_dev(div1, div0);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div0, null);
    			}

    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (!current || dirty & /*label*/ 4) set_data_dev(t0, /*label*/ ctx[2]);

    			if (dirty & /*round, contenders*/ 3) {
    				each_value = Array(/*round*/ ctx[0]);
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$1(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block$1(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(div0, null);
    					}
    				}

    				group_outros();

    				for (i = each_value.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}

    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;

    			for (let i = 0; i < each_value.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			current = true;
    		},
    		o: function outro(local) {
    			each_blocks = each_blocks.filter(Boolean);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div1);
    			destroy_each(each_blocks, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$2.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$2($$self, $$props, $$invalidate) {
    	let $lang;
    	validate_store(lang, "lang");
    	component_subscribe($$self, lang, $$value => $$invalidate(3, $lang = $$value));
    	let { round } = $$props;
    	let { contenders } = $$props;
    	const writable_props = ["round", "contenders"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Round> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Round", $$slots, []);

    	function pairClick_handler(event) {
    		bubble($$self, event);
    	}

    	$$self.$set = $$props => {
    		if ("round" in $$props) $$invalidate(0, round = $$props.round);
    		if ("contenders" in $$props) $$invalidate(1, contenders = $$props.contenders);
    	};

    	$$self.$capture_state = () => ({
    		lang,
    		Pair,
    		round,
    		contenders,
    		label,
    		$lang
    	});

    	$$self.$inject_state = $$props => {
    		if ("round" in $$props) $$invalidate(0, round = $$props.round);
    		if ("contenders" in $$props) $$invalidate(1, contenders = $$props.contenders);
    		if ("label" in $$props) $$invalidate(2, label = $$props.label);
    	};

    	let label;

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*$lang, round*/ 9) {
    			 $$invalidate(2, label = ({
    				en: {
    					8: "Octofinals",
    					4: "Quarterfinals",
    					2: "Semifinals",
    					1: "Final"
    				},
    				fr: {
    					8: "Huitièmes",
    					4: "Quarts",
    					2: "Demi-finales",
    					1: "Finale"
    				},
    				pt: {
    					8: "Oitavas",
    					4: "Quartas",
    					2: "Semifinais",
    					1: "Final"
    				}
    			})[$lang][round]);
    		}
    	};

    	return [round, contenders, label, $lang, pairClick_handler];
    }

    class Round extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$2, create_fragment$2, safe_not_equal, { round: 0, contenders: 1 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Round",
    			options,
    			id: create_fragment$2.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*round*/ ctx[0] === undefined && !("round" in props)) {
    			console.warn("<Round> was created without expected prop 'round'");
    		}

    		if (/*contenders*/ ctx[1] === undefined && !("contenders" in props)) {
    			console.warn("<Round> was created without expected prop 'contenders'");
    		}
    	}

    	get round() {
    		throw new Error("<Round>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set round(value) {
    		throw new Error("<Round>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get contenders() {
    		throw new Error("<Round>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set contenders(value) {
    		throw new Error("<Round>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/Champion.svelte generated by Svelte v3.22.2 */
    const file$3 = "src/Champion.svelte";

    // (5:0) {#if $champion}
    function create_if_block(ctx) {
    	let div3;
    	let div0;
    	let t1;
    	let div1;
    	let t2;
    	let t3;
    	let div2;

    	const block = {
    		c: function create() {
    			div3 = element("div");
    			div0 = element("div");
    			div0.textContent = "🏆";
    			t1 = space();
    			div1 = element("div");
    			t2 = text(/*$champion*/ ctx[0]);
    			t3 = space();
    			div2 = element("div");
    			div2.textContent = "🏆";
    			attr_dev(div0, "class", "trophy svelte-dmm82i");
    			add_location(div0, file$3, 6, 4, 106);
    			attr_dev(div1, "class", "name svelte-dmm82i");
    			add_location(div1, file$3, 7, 4, 139);
    			attr_dev(div2, "class", "trophy svelte-dmm82i");
    			add_location(div2, file$3, 8, 4, 179);
    			attr_dev(div3, "class", "champion svelte-dmm82i");
    			add_location(div3, file$3, 5, 2, 79);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div3, anchor);
    			append_dev(div3, div0);
    			append_dev(div3, t1);
    			append_dev(div3, div1);
    			append_dev(div1, t2);
    			append_dev(div3, t3);
    			append_dev(div3, div2);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*$champion*/ 1) set_data_dev(t2, /*$champion*/ ctx[0]);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div3);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block.name,
    		type: "if",
    		source: "(5:0) {#if $champion}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$3(ctx) {
    	let if_block_anchor;
    	let if_block = /*$champion*/ ctx[0] && create_if_block(ctx);

    	const block = {
    		c: function create() {
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			if (if_block) if_block.m(target, anchor);
    			insert_dev(target, if_block_anchor, anchor);
    		},
    		p: function update(ctx, [dirty]) {
    			if (/*$champion*/ ctx[0]) {
    				if (if_block) {
    					if_block.p(ctx, dirty);
    				} else {
    					if_block = create_if_block(ctx);
    					if_block.c();
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (if_block) if_block.d(detaching);
    			if (detaching) detach_dev(if_block_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$3.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$3($$self, $$props, $$invalidate) {
    	let $champion;
    	validate_store(champion, "champion");
    	component_subscribe($$self, champion, $$value => $$invalidate(0, $champion = $$value));
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Champion> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Champion", $$slots, []);
    	$$self.$capture_state = () => ({ champion, $champion });
    	return [$champion];
    }

    class Champion extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$3, create_fragment$3, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Champion",
    			options,
    			id: create_fragment$3.name
    		});
    	}
    }

    /* src/Match.svelte generated by Svelte v3.22.2 */
    const file$4 = "src/Match.svelte";

    // (13:0) {#if $screen == 'match'}
    function create_if_block$1(ctx) {
    	let div3;
    	let div0;
    	let t0_value = /*contenders*/ ctx[0][0] + "";
    	let t0;
    	let t1;
    	let div1;
    	let t3;
    	let div2;
    	let t4_value = /*contenders*/ ctx[0][1] + "";
    	let t4;
    	let dispose;

    	const block = {
    		c: function create() {
    			div3 = element("div");
    			div0 = element("div");
    			t0 = text(t0_value);
    			t1 = space();
    			div1 = element("div");
    			div1.textContent = "VS";
    			t3 = space();
    			div2 = element("div");
    			t4 = text(t4_value);
    			attr_dev(div0, "class", "contender svelte-1ytmaqh");
    			add_location(div0, file$4, 14, 6, 319);
    			attr_dev(div1, "class", "versus svelte-1ytmaqh");
    			add_location(div1, file$4, 15, 6, 415);
    			attr_dev(div2, "class", "contender svelte-1ytmaqh");
    			add_location(div2, file$4, 16, 6, 450);
    			attr_dev(div3, "class", "match svelte-1ytmaqh");
    			add_location(div3, file$4, 13, 2, 293);
    		},
    		m: function mount(target, anchor, remount) {
    			insert_dev(target, div3, anchor);
    			append_dev(div3, div0);
    			append_dev(div0, t0);
    			append_dev(div3, t1);
    			append_dev(div3, div1);
    			append_dev(div3, t3);
    			append_dev(div3, div2);
    			append_dev(div2, t4);
    			if (remount) run_all(dispose);

    			dispose = [
    				listen_dev(
    					div0,
    					"click",
    					function () {
    						if (is_function(/*onContenderClick*/ ctx[2](/*contenders*/ ctx[0][0]))) /*onContenderClick*/ ctx[2](/*contenders*/ ctx[0][0]).apply(this, arguments);
    					},
    					false,
    					false,
    					false
    				),
    				listen_dev(
    					div2,
    					"click",
    					function () {
    						if (is_function(/*onContenderClick*/ ctx[2](/*contenders*/ ctx[0][1]))) /*onContenderClick*/ ctx[2](/*contenders*/ ctx[0][1]).apply(this, arguments);
    					},
    					false,
    					false,
    					false
    				)
    			];
    		},
    		p: function update(new_ctx, dirty) {
    			ctx = new_ctx;
    			if (dirty & /*contenders*/ 1 && t0_value !== (t0_value = /*contenders*/ ctx[0][0] + "")) set_data_dev(t0, t0_value);
    			if (dirty & /*contenders*/ 1 && t4_value !== (t4_value = /*contenders*/ ctx[0][1] + "")) set_data_dev(t4, t4_value);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div3);
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$1.name,
    		type: "if",
    		source: "(13:0) {#if $screen == 'match'}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$4(ctx) {
    	let if_block_anchor;
    	let if_block = /*$screen*/ ctx[1] == "match" && create_if_block$1(ctx);

    	const block = {
    		c: function create() {
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			if (if_block) if_block.m(target, anchor);
    			insert_dev(target, if_block_anchor, anchor);
    		},
    		p: function update(ctx, [dirty]) {
    			if (/*$screen*/ ctx[1] == "match") {
    				if (if_block) {
    					if_block.p(ctx, dirty);
    				} else {
    					if_block = create_if_block$1(ctx);
    					if_block.c();
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (if_block) if_block.d(detaching);
    			if (detaching) detach_dev(if_block_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$4.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$4($$self, $$props, $$invalidate) {
    	let $screen;
    	validate_store(screen, "screen");
    	component_subscribe($$self, screen, $$value => $$invalidate(1, $screen = $$value));
    	const dispatch = createEventDispatcher();
    	let { contenders } = $$props;

    	function onContenderClick(contender) {
    		dispatch("contenderClick", { contender });
    	}

    	const writable_props = ["contenders"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Match> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Match", $$slots, []);

    	$$self.$set = $$props => {
    		if ("contenders" in $$props) $$invalidate(0, contenders = $$props.contenders);
    	};

    	$$self.$capture_state = () => ({
    		createEventDispatcher,
    		dispatch,
    		screen,
    		contenders,
    		onContenderClick,
    		$screen
    	});

    	$$self.$inject_state = $$props => {
    		if ("contenders" in $$props) $$invalidate(0, contenders = $$props.contenders);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [contenders, $screen, onContenderClick];
    }

    class Match extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$4, create_fragment$4, safe_not_equal, { contenders: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Match",
    			options,
    			id: create_fragment$4.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*contenders*/ ctx[0] === undefined && !("contenders" in props)) {
    			console.warn("<Match> was created without expected prop 'contenders'");
    		}
    	}

    	get contenders() {
    		throw new Error("<Match>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set contenders(value) {
    		throw new Error("<Match>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/App.svelte generated by Svelte v3.22.2 */

    const { Object: Object_1 } = globals;
    const file$5 = "src/App.svelte";

    function get_each_context$2(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[10] = list[i];
    	return child_ctx;
    }

    // (32:0) {:else}
    function create_else_block(ctx) {
    	let main;
    	let div;
    	let t0;
    	let t1;
    	let current;
    	let each_value = /*rounds*/ ctx[4];
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$2(get_each_context$2(ctx, each_value, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	const champion_1 = new Champion({ $$inline: true });

    	const match_1 = new Match({
    			props: { contenders: /*match*/ ctx[1].contenders },
    			$$inline: true
    		});

    	match_1.$on("contenderClick", /*onContenderClick*/ ctx[6]);

    	const block = {
    		c: function create() {
    			main = element("main");
    			div = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t0 = space();
    			create_component(champion_1.$$.fragment);
    			t1 = space();
    			create_component(match_1.$$.fragment);
    			attr_dev(div, "class", "whitening svelte-15sao0s");
    			add_location(div, file$5, 33, 4, 980);
    			set_style(main, "background-image", "url(" + /*$background*/ ctx[3] + ")");
    			attr_dev(main, "class", "svelte-15sao0s");
    			add_location(main, file$5, 32, 2, 924);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, main, anchor);
    			append_dev(main, div);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}

    			append_dev(div, t0);
    			mount_component(champion_1, div, null);
    			append_dev(div, t1);
    			mount_component(match_1, div, null);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*rounds, contenders, onPairClick*/ 49) {
    				each_value = /*rounds*/ ctx[4];
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$2(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block$2(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(div, t0);
    					}
    				}

    				group_outros();

    				for (i = each_value.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}

    				check_outros();
    			}

    			const match_1_changes = {};
    			if (dirty & /*match*/ 2) match_1_changes.contenders = /*match*/ ctx[1].contenders;
    			match_1.$set(match_1_changes);

    			if (!current || dirty & /*$background*/ 8) {
    				set_style(main, "background-image", "url(" + /*$background*/ ctx[3] + ")");
    			}
    		},
    		i: function intro(local) {
    			if (current) return;

    			for (let i = 0; i < each_value.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			transition_in(champion_1.$$.fragment, local);
    			transition_in(match_1.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			each_blocks = each_blocks.filter(Boolean);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			transition_out(champion_1.$$.fragment, local);
    			transition_out(match_1.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(main);
    			destroy_each(each_blocks, detaching);
    			destroy_component(champion_1);
    			destroy_component(match_1);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block.name,
    		type: "else",
    		source: "(32:0) {:else}",
    		ctx
    	});

    	return block;
    }

    // (30:0) {#if $initialContenders.length == 1}
    function create_if_block$2(ctx) {
    	let current;
    	const examples = new Examples({ $$inline: true });

    	const block = {
    		c: function create() {
    			create_component(examples.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(examples, target, anchor);
    			current = true;
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(examples.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(examples.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(examples, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$2.name,
    		type: "if",
    		source: "(30:0) {#if $initialContenders.length == 1}",
    		ctx
    	});

    	return block;
    }

    // (35:6) {#each rounds as r}
    function create_each_block$2(ctx) {
    	let current;

    	const round = new Round({
    			props: {
    				round: /*r*/ ctx[10],
    				contenders: /*contenders*/ ctx[0][/*r*/ ctx[10]]
    			},
    			$$inline: true
    		});

    	round.$on("pairClick", /*onPairClick*/ ctx[5]);

    	const block = {
    		c: function create() {
    			create_component(round.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(round, target, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const round_changes = {};
    			if (dirty & /*contenders*/ 1) round_changes.contenders = /*contenders*/ ctx[0][/*r*/ ctx[10]];
    			round.$set(round_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(round.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(round.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(round, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$2.name,
    		type: "each",
    		source: "(35:6) {#each rounds as r}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$5(ctx) {
    	let current_block_type_index;
    	let if_block;
    	let if_block_anchor;
    	let current;
    	const if_block_creators = [create_if_block$2, create_else_block];
    	const if_blocks = [];

    	function select_block_type(ctx, dirty) {
    		if (/*$initialContenders*/ ctx[2].length == 1) return 0;
    		return 1;
    	}

    	current_block_type_index = select_block_type(ctx);
    	if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);

    	const block = {
    		c: function create() {
    			if_block.c();
    			if_block_anchor = empty();
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			if_blocks[current_block_type_index].m(target, anchor);
    			insert_dev(target, if_block_anchor, anchor);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			let previous_block_index = current_block_type_index;
    			current_block_type_index = select_block_type(ctx);

    			if (current_block_type_index === previous_block_index) {
    				if_blocks[current_block_type_index].p(ctx, dirty);
    			} else {
    				group_outros();

    				transition_out(if_blocks[previous_block_index], 1, 1, () => {
    					if_blocks[previous_block_index] = null;
    				});

    				check_outros();
    				if_block = if_blocks[current_block_type_index];

    				if (!if_block) {
    					if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    					if_block.c();
    				}

    				transition_in(if_block, 1);
    				if_block.m(if_block_anchor.parentNode, if_block_anchor);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if_blocks[current_block_type_index].d(detaching);
    			if (detaching) detach_dev(if_block_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$5.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$5($$self, $$props, $$invalidate) {
    	let $title;
    	let $initialContenders;
    	let $screen;
    	let $champion;
    	let $background;
    	validate_store(title, "title");
    	component_subscribe($$self, title, $$value => $$invalidate(7, $title = $$value));
    	validate_store(initialContenders, "initialContenders");
    	component_subscribe($$self, initialContenders, $$value => $$invalidate(2, $initialContenders = $$value));
    	validate_store(screen, "screen");
    	component_subscribe($$self, screen, $$value => $$invalidate(8, $screen = $$value));
    	validate_store(champion, "champion");
    	component_subscribe($$self, champion, $$value => $$invalidate(9, $champion = $$value));
    	validate_store(background, "background");
    	component_subscribe($$self, background, $$value => $$invalidate(3, $background = $$value));
    	document.title = $title + "!";
    	let rounds = [8, 4, 2, 1]; // [...Array(4).keys()].map(i => Math.pow(2, i)).reverse()
    	let contenders = Object.fromEntries(rounds.map(n => [n, []])); // rounds.reduce((o, k) => (o[k] = [], o), {})
    	contenders[8] = $initialContenders;
    	let match = { contenders: [], round: 8, pair: 1 };

    	function onPairClick(e) {
    		$$invalidate(1, match = e.detail);
    		set_store_value(screen, $screen = "match");
    	}

    	function onContenderClick(e) {
    		let c = e.detail.contender;
    		if (match.round != 1) $$invalidate(0, contenders[match.round / 2][match.pair - 1] = c, contenders); else set_store_value(champion, $champion = c);
    		set_store_value(screen, $screen = "tournament");
    	}

    	const writable_props = [];

    	Object_1.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<App> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("App", $$slots, []);

    	$$self.$capture_state = () => ({
    		lang,
    		title,
    		background,
    		initialContenders,
    		screen,
    		champion,
    		Examples,
    		Round,
    		Champion,
    		Match,
    		rounds,
    		contenders,
    		match,
    		onPairClick,
    		onContenderClick,
    		$title,
    		$initialContenders,
    		$screen,
    		$champion,
    		$background
    	});

    	$$self.$inject_state = $$props => {
    		if ("rounds" in $$props) $$invalidate(4, rounds = $$props.rounds);
    		if ("contenders" in $$props) $$invalidate(0, contenders = $$props.contenders);
    		if ("match" in $$props) $$invalidate(1, match = $$props.match);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [
    		contenders,
    		match,
    		$initialContenders,
    		$background,
    		rounds,
    		onPairClick,
    		onContenderClick
    	];
    }

    class App extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$5, create_fragment$5, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "App",
    			options,
    			id: create_fragment$5.name
    		});
    	}
    }

    const app = new App({
    	target: document.body,
    	props: {}
    });

    return app;

}());
//# sourceMappingURL=bundle.js.map
