import { get, readable, writable } from 'svelte/store'

const params = new URLSearchParams(window.location.search)

export const lang = readable(params.get('l') || 'en')

export const title = readable({en: 'Knockout', fr: 'Ti-coupe', pt: 'Copinha'}[get(lang)])

export const background = readable(params.get('bg'))

export const initialContenders = readable(
  decodeURIComponent(params.get('c') || '')
    .replace(/^\?/, '')
    .split('|')
)

export const screen = writable('tournament')

export const champion = writable()
